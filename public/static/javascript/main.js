/**
 * 	By: Karl Löfquist
 */

// Declaring variables to represent html-elements
let restaurantNameElem;
let restaurantImageElem;
let restaurantAddressElem;
let restaurantDescriptionElem;
let restaurantRatingElem;
let reviewTableElem;

/*
*	Function that runs when the entire restaurant.html has been loaded
*/
function init() {

	// Initiating elements related to restaurant information
	restaurantNameElem = document.getElementById('restaurantName');
	restaurantImageElem = document.getElementById('restaurantImage');
	restaurantAddressElem = document.getElementById('restaurantAddress');
	restaurantDescriptionElem = document.getElementById('restaurantDescription');
	restaurantRatingElem = document.getElementById('restaurantRating');
	reviewTableElem = document.getElementById('reviewTable');

	// Fetching json data from the data-endpoint (data.json)
	fetch('./data')
		.then(response => response.json())
		// Sends recieved json to function printRestaurantInfo
	  	.then(data => printRestaurantInfo(data));

}

// Adding event listener to window object to call the init() function when the index page has been loaded
window.addEventListener('load', init);

/*
*	Function that prints basic data from json to the html file
*/
function printRestaurantInfo(data) {

	restaurantNameElem.innerHTML = data.name;
	restaurantImageElem.src = 'assets/images/' + data.imageURL;
	restaurantAddressElem.innerHTML = '<b>Address:</b> ' + data.address;
	restaurantDescription.innerHTML = '<b>Description:</b> ' + data.description;
	restaurantRatingElem.innerHTML = '<b>Rating:</b> ' + data.rating + '/5 stars';

	// Show review table
	reviewTableElem.style.display = 'block';

	// Calls function printToReviewsTable for each object in the reviews json array
	data.reviews.forEach(review => printToReviewsTable(review));

}

/*
*	Function that creates table elements for each review and populates them with recieved data
*/
function printToReviewsTable(review) {
	let trTag = document.createElement('tr');

	let tdAuthorTag = document.createElement('td');
	tdAuthorTag.innerHTML = review.author;
	trTag.append(tdAuthorTag);

	let tdContentTag = document.createElement('td');
	tdContentTag.innerHTML = review.content;
	trTag.append(tdContentTag);

	reviewTableElem.append(trTag);

}