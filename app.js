/**
 * 	By: Karl Löfquist
 */

// Necessary constants
const express = require('express');
const app = express();
const { PORT = 8080 } = process.env;
const path = require('path');
const data = require('./data.json');

// Sets handy usages in app
app.use('/assets', express.static(path.join(__dirname, 'public', 'static')));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Endpoint for root ('/')
app.get('/', (req, res) => {
	// Return file (index.html)
	return res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Endpoint for restaurant ('/restaurant')
app.get('/restaurant', (req, res) => {
	// Return file (restaurant.html)
	return res.sendFile(path.join(__dirname, 'public', 'restaurant.html'));
});

// Endpoint for data ('/data')
app.get('/data', (req, res) => {
	// Return JSON-data of restaurant
	return res.status(200).json(data);
});

app.listen(PORT, ()=> console.log(`Server started on port ${PORT}...`));